<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::prefix('categories')->namespace('Product')->group(function () {
        Route::get('', 'CategoryController@index');
        Route::post('', 'CategoryController@create');
        Route::put('/{id}', 'CategoryController@update');
        Route::delete('/{id}', 'CategoryController@delete');
    });

    Route::prefix('images')->namespace('Product')->group(function () {
        Route::get('', 'ImageController@index');
        Route::post('', 'ImageController@create');
        Route::put('/{id}', 'ImageController@update');
        Route::delete('/{id}', 'ImageController@delete');
    });

    Route::prefix('products')->namespace('Product')->group(function () {
        Route::get('', 'ProductController@index');
        Route::post('', 'ProductController@create');
        Route::put('/{id}', 'ProductController@update');
        Route::delete('/{id}', 'ProductController@delete');
    });
});
