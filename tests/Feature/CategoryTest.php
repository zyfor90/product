<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Category;

class CategoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetCategoriesSuccess()
    {
        $response = $this->get('/api/v1/categories');

        $response->assertStatus(200);
    }
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateCategorySuccess()
    {
        $test = Category::where('name', 'TestingCategory')->first();
        if ($test) {
            Category::where('name', 'TestingCategory')->delete();
        }

        $response = $this->post('/api/v1/categories', [
            'name' => 'TestingCategory',
            'enable' => true,
        ]);

        $response->assertStatus(200);
    }

        /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateCategoryDuplicateName()
    {
        $response = $this->post('/api/v1/categories', [
            'name' => 'TestingCategory',
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateCategoryNameRequired()
    {
        $response = $this->post('/api/v1/categories', [
            'name' => '',
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

        /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateCategoryNameMinThreeCharacter()
    {
        $response = $this->post('/api/v1/categories', [
            'name' => '12',
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

     /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdateCategorySuccess()
    {
        $test = Category::where('name', 'UpdatedTestingCategory')->first();
        if ($test) {
            Category::where('name', 'UpdatedTestingCategory')->delete();
        }
        
        $category = Category::where('name', 'TestingCategory')->first();

        $response = $this->post('/api/v1/categories/' . $category->id . '?_method=PUT', [
            'name' => 'UpdatedTestingCategory',
            'enable' => true,
        ]);

        $response->assertStatus(200);
    }

    public function testUpdateCategoryDuplicateName()
    {
        $category = Category::where('name', 'UpdatedTestingCategory')->first();
        $getAnotherCategory = Category::where('id', 1)->first();
        $response = $this->post('/api/v1/categories/' . $category->id . '?_method=PUT', [
            'name' => $getAnotherCategory->name,
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateCategoryWithWrongId()
    {
        $response = $this->post('/api/v1/categories/' . 9999999999999999 . '?_method=PUT', [
            'name' => 'testing',
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateCategoryNameRequired()
    {
        $category = Category::where('name', 'UpdatedTestingCategory')->first();
        $response = $this->post('/api/v1/categories/' . $category->id . '?_method=PUT', [
            'name' => '',
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateCategoryNameMinThreeCharacter()
    {
        $category = Category::where('name', 'UpdatedTestingCategory')->first();
        $response = $this->post('/api/v1/categories/' . $category->id . '?_method=PUT', [
            'name' => '12',
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testDeleteCategorySuccess()
    {
        $category = Category::where('name', 'UpdatedTestingCategory')->first();
        $response = $this->delete('/api/v1/categories/' . $category->id);

        $response->assertStatus(200);
    }

    public function testDeleteCategoryWithWrongId()
    {
        $response = $this->delete('/api/v1/categories/' . 9999999999999999);

        $response->assertStatus(422);
    }
}
