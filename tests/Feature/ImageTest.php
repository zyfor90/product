<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Models\Image;
use Tests\TestCase;

class ImageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetImagesSuccess()
    {
        $response = $this->get('/api/v1/images');

        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateImageSuccess()
    {
        $image = Image::where('name', 'TestingPhoto')->first();
        if ($image) {
            // Remove Data
            Image::where('name', 'TestingPhoto')->delete();

            // * Remove unused image
            Storage::disk('public')->delete($image->file);
        }

        $file = UploadedFile::fake()->image('avatar.jpg');

        $response = $this->post('/api/v1/images', [
            'name' => 'TestingPhoto',
            'file' => $file,
            'enable' => true,
        ]);

        // * Check Existing image after data refreshed
        $image = Image::where('name', 'TestingPhoto')->first();
        Storage::disk('public')->assertExists($image->file);

        $response->assertStatus(200);
    }

    public function testCreateImageFileRequired()
    {
        $response = $this->post('/api/v1/images', [
            'name' => 'TestingPhoto',
            'file' => '',
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testCreateImageFileExtension()
    {
        $file = UploadedFile::fake()->image('avatar.doc');

        $response = $this->post('/api/v1/images', [
            'name' => 'TestingPhoto',
            'file' => $file,
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testCreateImageFileSize()
    {
        $file = UploadedFile::fake()->create('avatar.jpg', 5242880);

        $response = $this->post('/api/v1/images', [
            'name' => 'TestingPhoto',
            'file' => $file,
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

        /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdateImageSuccess()
    {
        $image = Image::where('name', 'TestingPhoto')->first();
        $file = UploadedFile::fake()->image('avatar.jpg');

        $response = $this->post('/api/v1/images/' . $image->id . '?_method=PUT', [
            'name' => 'TestingPhoto',
            'file' => $file,
            'enable' => true,
        ]);

        // * Check Existing image after data refreshed
        $image = Image::where('name', 'TestingPhoto')->first();
        Storage::disk('public')->assertExists($image->file);

        $response->assertStatus(200);
    }

    public function testUpdateImageWithWrongId()
    {
        $file = UploadedFile::fake()->image('avatar.jpg');
        $response = $this->post('/api/v1/images/' . 99999999999 . '?_method=PUT', [
            'name' => 'TestingPhoto',
            'file' => $file,
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateImageFileRequired()
    {
        $image = Image::where('name', 'TestingPhoto')->first();
        $response = $this->post('/api/v1/images/' . $image->id . '?_method=PUT', [
            'name' => 'TestingPhoto',
            'file' => '',
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateImageFileExtension()
    {
        $image = Image::where('name', 'TestingPhoto')->first();
        $file = UploadedFile::fake()->image('avatar.doc');

        $response = $this->post('/api/v1/images/' . $image->id . '?_method=PUT', [
            'name' => 'TestingPhoto',
            'file' => $file,
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateImageFileSize()
    {
        $image = Image::where('name', 'TestingPhoto')->first();
        $file = UploadedFile::fake()->create('avatar.jpg', 5242880);

        $response = $this->post('/api/v1/images/' . $image->id . '?_method=PUT', [
            'name' => 'TestingPhoto',
            'file' => $file,
            'enable' => true,
        ]);

        $response->assertStatus(422);
    }

    public function testDeleteImageSuccess()
    {
        $image = Image::where('name', 'TestingPhoto')->first();
        $response = $this->delete('/api/v1/images/' . $image->id);

        $response->assertStatus(200);
    }

    public function testDeleteImageWithWrongId()
    {
        $response = $this->delete('/api/v1/images/' . 9999999999999999);

        $response->assertStatus(422);
    }
}
