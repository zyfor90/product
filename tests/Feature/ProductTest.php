<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Product;
use App\Models\Category;
use App\Models\Image;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetProductsSuccess()
    {
        $response = $this->get('/api/v1/products');

        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateProductSuccess()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();
        if ($getProduct) {
            Product::where('name', 'TestingProduct')->delete();
        }       

        $categories = [];
        $getCategories = Category::limit(2)->get();
        if (count($getCategories) > 0) {
            foreach ($getCategories as $key => $category) {
                array_push($categories, [
                    'category_id' => $category->id,
                ]);
            }
        }
        
        $images = [];
        $getImages = Image::limit(2)->get();
        if (count($getImages) > 0) {
            foreach ($getImages as $key => $image) {
                array_push($images, [
                    'image_id' => $image->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => $categories,
            'images' => $images 
        ]);

        $response->assertStatus(200);
    }

        /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateProductSuccessWithEmptyCategories()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();
        if ($getProduct) {
            Product::where('name', 'TestingProduct')->delete();
        }       

        $images = [];
        $getImages = Image::limit(2)->get();
        if (count($getImages) > 0) {
            foreach ($getImages as $key => $image) {
                array_push($images, [
                    'image_id' => $image->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => $images 
        ]);

        $response->assertStatus(200);
    }

    public function testCreateProductSuccessWithEmptyImages()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();
        if ($getProduct) {
            Product::where('name', 'TestingProduct')->delete();
        }       

        $categories = [];
        $getCategories = Category::limit(2)->get();
        if (count($getCategories) > 0) {
            foreach ($getCategories as $key => $category) {
                array_push($categories, [
                    'category_id' => $category->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => $categories,
            'images' => [] 
        ]);

        $response->assertStatus(200);
    }


    public function testCreateProductNameRequired()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();     

        $categories = [];
        $getCategories = Category::limit(2)->get();
        if (count($getCategories) > 0) {
            foreach ($getCategories as $key => $category) {
                array_push($categories, [
                    'category_id' => $category->id,
                ]);
            }
        }
        
        $images = [];
        $getImages = Image::limit(2)->get();
        if (count($getImages) > 0) {
            foreach ($getImages as $key => $image) {
                array_push($images, [
                    'image_id' => $image->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products', [
            'name' => '',
            'description' => 'testing description',
            'enable' => true,
            'categories' => $categories,
            'images' => $images 
        ]);

        $response->assertStatus(422);
    }

    public function testCreateProductNameMinThreeCharacter()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();    

        $categories = [];
        $getCategories = Category::limit(2)->get();
        if (count($getCategories) > 0) {
            foreach ($getCategories as $key => $category) {
                array_push($categories, [
                    'category_id' => $category->id,
                ]);
            }
        }
        
        $images = [];
        $getImages = Image::limit(2)->get();
        if (count($getImages) > 0) {
            foreach ($getImages as $key => $image) {
                array_push($images, [
                    'image_id' => $image->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products', [
            'name' => '12',
            'description' => 'testing description',
            'enable' => true,
            'categories' => $categories,
            'images' => $images 
        ]);

        $response->assertStatus(422);
    }
    
    public function testCreateProductWithStringCategoryId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();    

        $response = $this->post('/api/v1/products', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [
                [
                    'category_id' => 'testing',
                ]
            ],
            'images' => [] 
        ]);

        $response->assertStatus(422);
    }

    public function testCreateProductWithWrongCategoryId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();      

        $response = $this->post('/api/v1/products', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [
                [
                    'category_id' => 99999999,
                ]
            ],
            'images' => [] 
        ]);

        $response->assertStatus(422);
    }

    public function testCreateProductWithStringImageId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();     
        
        $response = $this->post('/api/v1/products', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => [
                [
                    'image_id' => 'testing'
                ]
            ],
        ]);

        $response->assertStatus(422);
    }

    public function testCreateProductWithWrongImageId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();      
        
        $response = $this->post('/api/v1/products', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => [
                [
                    'image_id' => 999999
                ]
            ] 
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateProductWithWrongId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();

        $response = $this->post('/api/v1/products/' . 99999999999 . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => []
        ]);

        $response->assertStatus(422);
    }

        /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdateProductSuccess()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();

        $categories = [];
        $getCategories = Category::limit(2)->get();
        if (count($getCategories) > 0) {
            foreach ($getCategories as $key => $category) {
                array_push($categories, [
                    'category_id' => $category->id,
                ]);
            }
        }
        
        $images = [];
        $getImages = Image::limit(2)->get();
        if (count($getImages) > 0) {
            foreach ($getImages as $key => $image) {
                array_push($images, [
                    'image_id' => $image->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => $categories,
            'images' => $images 
        ]);

        $response->assertStatus(200);
    }

        /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdateProductSuccessWithEmptyCategories()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();  

        $images = [];
        $getImages = Image::limit(2)->get();
        if (count($getImages) > 0) {
            foreach ($getImages as $key => $image) {
                array_push($images, [
                    'image_id' => $image->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => $images 
        ]);

        $response->assertStatus(200);
    }

    public function testUpdateProductSuccessWithEmptyImages()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();   

        $categories = [];
        $getCategories = Category::limit(2)->get();
        if (count($getCategories) > 0) {
            foreach ($getCategories as $key => $category) {
                array_push($categories, [
                    'category_id' => $category->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => $categories,
            'images' => [] 
        ]);

        $response->assertStatus(200);
    }


    public function testUpdateProductNameRequired()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();   

        $categories = [];
        $getCategories = Category::limit(2)->get();
        if (count($getCategories) > 0) {
            foreach ($getCategories as $key => $category) {
                array_push($categories, [
                    'category_id' => $category->id,
                ]);
            }
        }
        
        $images = [];
        $getImages = Image::limit(2)->get();
        if (count($getImages) > 0) {
            foreach ($getImages as $key => $image) {
                array_push($images, [
                    'image_id' => $image->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => '',
            'description' => 'testing description',
            'enable' => true,
            'categories' => $categories,
            'images' => $images 
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateProductNameMinThreeCharacter()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();   

        $categories = [];
        $getCategories = Category::limit(2)->get();
        if (count($getCategories) > 0) {
            foreach ($getCategories as $key => $category) {
                array_push($categories, [
                    'category_id' => $category->id,
                ]);
            }
        }
        
        $images = [];
        $getImages = Image::limit(2)->get();
        if (count($getImages) > 0) {
            foreach ($getImages as $key => $image) {
                array_push($images, [
                    'image_id' => $image->id,
                ]);
            }
        }

        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => '12',
            'description' => 'testing description',
            'enable' => true,
            'categories' => $categories,
            'images' => $images 
        ]);

        $response->assertStatus(422);
    }
    
    public function testUpdateProductWithStringCategoryId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();  

        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [
                [
                    'category_id' => 'testing',
                ]
            ],
            'images' => [] 
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateProductWithWrongCategoryId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();  

        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [
                [
                    'category_id' => 99999999,
                ]
            ],
            'images' => [] 
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateProductWithStringImageId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();
        
        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => [
                [
                    'image_id' => 'testing'
                ]
            ],
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateProductWithWrongImageId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();   
        
        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => [
                [
                    'image_id' => 999999
                ]
            ] 
        ]);

        $response->assertStatus(422);
    }

    public function testDeleteProductWithWrongId()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();

        $response = $this->post('/api/v1/products/' . 99999999999 . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => []
        ]);

        $response->assertStatus(422);
    }

    public function testDeleteProductSuccess()
    {
        $getProduct = Product::where('name', 'TestingProduct')->first();

        $response = $this->post('/api/v1/products/' . $getProduct->id . '?_method=PUT', [
            'name' => 'TestingProduct',
            'description' => 'testing description',
            'enable' => true,
            'categories' => [],
            'images' => []
        ]);

        $response->assertStatus(200);
    }
}
