<?php

namespace App\Traits;

use Log;

trait Helper
{

    public function sendLog($message)
    {
        Log::info('========== Start Internal Server Error ==========');
        Log::info($message);
        Log::info('=========== End Internal Server Error ==========');
    }
}
