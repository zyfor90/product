<?php

namespace App\Http\Requests\Image;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', // maksimum 2MB
            'enable' => 'required|boolean'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Silakan isi nama Gambar terlebih dahulu.',
            'name.min' => 'Nama Gambar harus terdiri dari minimum 3 karakter',
            'file.required' => 'Silakan upload file terlebih dahulu',
            'file.image' => 'File harus berbentuk gambar.',
            'file.mimes' => 'Format gambar harus jpeg,png,jpg,gif,svg',
            'file.max' => 'ukuran gambar maksimum adalah 2048 kg atau 2 mb',
            'enable.required' => 'Silakan isi field enable terlebih dahulu',
            'enable.boolean' => 'Silahkan isi field enable dengan 1 atau 0'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'meta' => [
                'code' => 422,
                'status' => 'error',
                'message' => $validator->errors()->first()
            ],
            'data' => []
        ], 422));
    }
}
