<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'enable' => 'required|boolean'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Silakan isi nama kategori terlebih dahulu.',
            'name.min' => 'Nama kategori harus terdiri dari minimum 3 karakter',
            'enable.required' => 'Silakan isi field enable terlebih dahulu',
            'enable.boolean' => 'Silahkan isi field enable dengan 1 atau 0'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'meta' => [
                'code' => 422,
                'status' => 'error',
                'message' => $validator->errors()->first()
            ],
            'data' => []
        ], 422));
    }
}
