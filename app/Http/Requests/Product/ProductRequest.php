<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'enable' => 'required|boolean',
            'categories.*.category_id' => ['integer', 'exists:categories,id'],
            'images.*.image_id' => ['integer', 'exists:images,id'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Silakan isi nama Produk terlebih dahulu.',
            'name.min' => 'Nama Produk harus terdiri dari minimum 3 karakter',
            'enable.required' => 'Silakan isi field enable terlebih dahulu',
            'enable.boolean' => 'Silahkan isi field enable dengan 1 atau 0',
            'categories.*.category_id.integer' => 'ID Kategori harus di isi dengan angka.',
            'categories.*.category_id.exists' => 'Silahkan isi ID kategori yang valid.',
            'images.*.image_id.integer' => 'ID Image harus di isi dengan angka.',
            'images.*.image_id.exists' => 'Silahkan isi ID Image yang valid.',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'meta' => [
                'code' => 422,
                'status' => 'error',
                'message' => $validator->errors()->first()
            ],
            'data' => []
        ], 422));
    }
}
