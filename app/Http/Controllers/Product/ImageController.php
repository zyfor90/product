<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Image;
use App\Http\Resources\Image\ImagesResource;
use App\Http\Requests\Image\ImageRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function index()
    {
        try {
            $images = Image::where('enable', true)->orderBy('id', 'desc')->get();
            if (count($images) == 0) {
                return $this->errorResponse('Saat ini belum ada data gambar', 422);
            }

            return $this->successResponse('Berhasil Mendapatkan Data Gambar', ImagesResource::collection($images), 200);
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function create(ImageRequest $request)
    {
        // try {
            $response = \DB::transaction(function() use ($request) {
                if (!$request->hasFile('file') && !$request->file('file')->isValid()) {
                    return $this->errorResponse('gagal mengupload gambar.', 400);
                }

                $path = $this->storeImage($request->file);

                // * Create new Image
                Image::create([
                    'name' => $request->name,
                    'file' => $path,
                    'enable' => $request->enable,
                ]);

                return $this->successResponse('Berhasil mengupload gambar', [], 200);
            });

            return $response;
        // } catch (\Throwable $th) {
        //     $this->sendLog($th->getMessage());
        //     return $this->errorResponse('Internal Server Error', 500);
        // }
    }

    public function update(ImageRequest $request, $id)
    {
        try {
            $response = \DB::transaction(function() use ($request, $id) {
                $image = Image::where('id', $id)->first();
                if(!$image) {
                    return $this->errorResponse('Gambar tidak ditemukan', 422);
                }

                if (!$request->hasFile('file') && !$request->file('file')->isValid()) {
                    return $this->errorResponse('gagal mengupload gambar.', 400);
                }

                $path = $this->storeImage($request->file);

                // * Update new Image
                Image::where('id', $id)->update([
                    'name' => $request->name,
                    'file' => $path,
                    'enable' => $request->enable,
                ]);

                // * Remove the previous images after image has changed but dont remove dummy image
                if($image->file != 'dummy/test.png'){
                    Storage::disk('public')->delete($image->file);
                }

                return $this->successResponse('Berhasil merubah gambar', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function storeImage($file)
    {
        // * Get File Extension
        $name = str::random(20) . '_' . date('Ymdhis') . '_' . time() . '.' . $file->getClientOriginalExtension();
        $path = 'images/' . $name;

        Storage::disk('public')->put('images/' . $name, 'Contents');
                
        return $path;
    }

    public function delete($id)
    {
        try {
            $response = \DB::transaction(function() use ($id) {
                $image = Image::where('id', $id)->first();
                if(!$image) {
                    return $this->errorResponse('Gambar tidak ditemukan', 422);
                }

                // * Soft Delete the data
                Image::where('id', $id)->delete();

                return $this->successResponse('Berhasil Menghapus Gambar', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }
}
