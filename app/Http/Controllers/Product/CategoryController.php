<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Resources\Category\CategoriesResource;
use App\Http\Requests\Category\CategoryRequest;

class CategoryController extends Controller
{
    public function index()
    {
        try {
            $categories = Category::where('enable', true)->orderBy('name', 'asc')->get();
            if (count($categories) == 0) {
                return $this->errorResponse('Saat ini belum ada data kategori', 422);
            }

            return $this->successResponse('Berhasil Mendapatkan Data Kategori', CategoriesResource::collection($categories), 200);
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function create(CategoryRequest $request)
    {
        try {
            $response = \DB::transaction(function() use ($request) {
                // * Check duplicate name with case sensitive.
                $category = Category::where(\DB::raw('LOWER(name)'), strtolower($request->name))->first();
                if($category) {
                    return $this->errorResponse('Category dengan nama ' . $request->name . ' sudah ada.', 422);
                }

                // * Create new Category
                Category::create([
                    'name' => $request->name,
                    'enable' => $request->enable,
                ]);
                return $this->successResponse('Berhasil membuat Kategori baru', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function update(CategoryRequest $request, $id)
    {
        try {
            $response = \DB::transaction(function() use ($request, $id) {
                $category = Category::where('id', $id)->first();
                if(!$category) {
                    return $this->errorResponse('Kategori tidak ditemukan', 422);
                }

                // * Check duplicate name with case sensitive.
                $category = Category::where('id', '!=', $id)->where(\DB::raw('LOWER(name)'), strtolower($request->name))->first();
                if($category) {
                    return $this->errorResponse('Category dengan nama ' . $request->name . ' sudah ada.', 422);
                }

                // * Update Category
                Category::where('id', $id)->update([
                    'name' => $request->name,
                    'enable' => $request->enable,
                ]);

                return $this->successResponse('Berhasil merubah nama kategori', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function delete($id)
    {
        try {
            $response = \DB::transaction(function() use ($id) {
                $category = Category::where('id', $id)->first();
                if(!$category) {
                    return $this->errorResponse('Kategori tidak ditemukan', 422);
                }

                // * Soft Delete the data
                Category::where('id', $id)->delete();

                return $this->successResponse('Berhasil Menghapus Kategori', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }
}
