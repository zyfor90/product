<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductCategory;
use App\Http\Resources\Product\ProductsResource;
use App\Http\Requests\Product\ProductRequest;

class ProductController extends Controller
{
    public function index()
    {
        try {
            // * Get All data with eager loading to solve N+1 Problem.
            $products = Product::where('enable', true)->orderBy('id', 'desc')->with(['categories', 'images'])->get();

            return $this->successResponse('Berhasil menampilkan product', ProductsResource::collection($products), 200);
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function create(ProductRequest $request)
    {
        try {
            $response = \DB::transaction(function() use ($request) {
                $product = Product::create([
                    'name' => $request->name,
                    'description' => $request->description,
                    'enable' => $request->enable,
                ]);

                if (isset($request->categories)) {
                    $generateProductCategory = $this->addProductIDToArray($request->categories, $product->id);
                    ProductCategory::insert($generateProductCategory);
                }

                if (isset($request->images)) {
                    $generateProductImage = $this->addProductIDToArray($request->images, $product->id);
                    ProductImage::insert($generateProductImage);
                }

                return $this->successResponse('Berhasil Menambahkan Product', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function update(ProductRequest $request, $id)
    {
        try {
            $response = \DB::transaction(function() use ($request, $id) {
                $product = Product::where('id', $id)->first();
                if(!$product) {
                    return $this->errorResponse('Product tidak ditemukan', 422);
                }

                $product = Product::where('id', $id)->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    'enable' => $request->enable,
                ]);

                ProductCategory::where('product_id', $id)->forceDelete();
                if (isset($request->categories)) {
                    $generateProductCategory = $this->addProductIDToArray($request->categories, $id);
                    ProductCategory::insert($generateProductCategory);
                }

                ProductImage::where('product_id', $id)->forceDelete();
                if (isset($request->images)) {
                    $generateProductImage = $this->addProductIDToArray($request->images, $id);
                    ProductImage::insert($generateProductImage);
                }

                return $this->successResponse('Berhasil Menambahkan Product', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }

    public function addProductIDToArray($data, $productID)
    {
        $array = $data;
        foreach ($array as $key => $value) {
            $array[$key]['product_id'] = $productID;
        }

        return $array;
    }

    public function delete($id)
    {
        try {
            $response = \DB::transaction(function() use ($id) {
                $product = Product::where('id', $id)->first();
                if(!$product) {
                    return $this->errorResponse('Product tidak ditemukan', 422);
                }

                // * Soft Delete the data
                Product::where('id', $id)->delete();
                ProductCategory::where('product_id', $id)->delete();
                ProductImage::where('product_id', $id)->delete();

                return $this->successResponse('Berhasil Menghapus Product', [], 200);
            });

            return $response;
        } catch (\Throwable $th) {
            $this->sendLog($th->getMessage());
            return $this->errorResponse('Internal Server Error', 500);
        }
    }
}
