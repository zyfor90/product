<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;

    protected $table = 'images';

    protected $fillable = ['name', 'file', 'enable'];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
