## Requirement

- composer versi 1 (mohon maaf waktu bikin project lupa di ganti ke versi 2)
- php versi 8.0.25
- laravel versi 7
- database (postgresql)

## Catatan

- tambahkan environment file di postman dengan base url (example: localhost:8000)
- kalo pake linux / macos tolong di kasih permission untuk create file ke folder public dan storage
- saya menggunakan image dummy di folder public/dummy
- saya menggunakan logging laravel (storage) untuk tracking bug dari try catch
- saya menggunakan requests, resource, try catch, db transaction, logging, softdeletes, storage and tests di project ini.

## File Pendukung

- Link Dokumentasi API : https://documenter.getpostman.com/view/11540468/2s93CNNYuM
- Link Postman Collection : https://api.postman.com/collections/11540468-166edcfe-16d2-4b90-9a20-31c3bc1aab09?access_key=PMAT-01GT14HYFW8MM4N6NWV60G76S9

## Menjalankan Laravel

- composer install
- php artisan key:generate
- ganti database env value di .env
- php artisan migrate
- php artisan db:seed
- php artisan storage:link
- php artisan serve

## Menjalankan Tests

- php artisan test

## Hallo

- Semoga memenuhi kriteria testing nya :D