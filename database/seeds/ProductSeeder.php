<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert([
            [
                'name' => 'Baju Anak',
                'description' => 'desc baju anak',
                'enable' => true
            ],
            [
                'name' => 'Baju dan Celana Anak',
                'description' => 'desc baju dan celana anak',
                'enable' => true
            ],
            [
                'name' => 'Celana dewasa',
                'description' => 'desc celana anak',
                'enable' => true
            ],
            [
                'name' => 'Sendal Anak',
                'description' => 'desc celana anak',
                'enable' => true
            ],
        ]);

        ProductCategory::insert([
            [
                'category_id' => 2,
                'product_id' => 1,
            ],
            [
                'category_id' => 2,
                'product_id' => 2,
            ],
            [
                'category_id' => 4,
                'product_id' => 2,
            ],
            [
                'category_id' => 4,
                'product_id' => 3,
            ],
            [
                'category_id' => 8,
                'product_id' => 4,
            ],
        ]);

        ProductImage::insert([
            [
                'image_id' => 1,
                'product_id' => 1,
            ],
            [
                'image_id' => 2,
                'product_id' => 2,
            ],
            [
                'image_id' => 3,
                'product_id' => 2,
            ],
            [
                'image_id' => 2,
                'product_id' => 3,
            ],
            [
                'image_id' => 1,
                'product_id' => 4,
            ],
            [
                'image_id' => 2,
                'product_id' => 4,
            ],
            [
                'image_id' => 3,
                'product_id' => 4,
            ],
        ]);
    }
}
