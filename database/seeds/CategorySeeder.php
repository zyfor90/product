<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            [
                'name' => 'Perhiasan',
                'enable' => true,
            ],
            [
                'name' => 'Baju',
                'enable' => true,
            ],
            [
                'name' => 'Jaket',
                'enable' => true
            ],
            [
                'name' => 'Celana',
                'enable' => true
            ],
            [
                'name' => 'Topi',
                'enable' => true
            ],
            [
                'name' => 'Tas',
                'enable' => true
            ],
            [
                'name' => 'Sepatu',
                'enable' => true
            ],
            [
                'name' => 'Sendal',
                'enable' => true
            ]
        ]);
    }
}
