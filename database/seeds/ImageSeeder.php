<?php

use Illuminate\Database\Seeder;
use App\Models\Image;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Image::insert([
            [
                'name' => 'Gambar 1',
                'file' => 'dummy/test.png',
                'enable' => true,
            ],
            [
                'name' => 'Gambar 2',
                'file' => 'dummy/test.png',
                'enable' => true,
            ],
            [
                'name' => 'Gambar 3',
                'file' => 'dummy/test.png',
                'enable' => true
            ],
        ]);
    }
}
